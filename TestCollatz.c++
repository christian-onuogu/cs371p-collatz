// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

TEST(CollatzFixture, read2) {
    string s("3 4\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   3);
    ASSERT_EQ(p.second,  4);
}

TEST(CollatzFixture, read3) {
    string s("34 67\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,  34);
    ASSERT_EQ(p.second, 67);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(9, 2);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(8, 8);
    ASSERT_EQ(v, 4);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(6, 72);
    ASSERT_EQ(v, 113);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(61, 91);
    ASSERT_EQ(v, 116);
}

TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(74, 85);
    ASSERT_EQ(v, 111);
}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(99, 641);
    ASSERT_EQ(v, 144);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(92, 174);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(634, 913);
    ASSERT_EQ(v, 179);
}

TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(354, 361);
    ASSERT_EQ(v, 51);
}

TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(90, 330);
    ASSERT_EQ(v, 144);
}

TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(4829, 9120);
    ASSERT_EQ(v, 262);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

TEST(CollatzFixture, print2) {
    ostringstream w;
    collatz_print(w, 35, 89, 116);
    ASSERT_EQ(w.str(), "35 89 116\n");
}

TEST(CollatzFixture, print3) {
    ostringstream w;
    collatz_print(w, 783, 896, 179);
    ASSERT_EQ(w.str(), "783 896 179\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}

TEST(CollatzFixture, solve2) {
    istringstream r("3 4\n34 67\n27 86\n53 85\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("3 4 8\n34 67 113\n27 86 116\n53 85 116\n", w.str());
}

TEST(CollatzFixture, solve3) {
    istringstream r("35 89\n234 674\n354 568\n783 896\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("35 89 116\n234 674 145\n354 568 142\n783 896 179\n", w.str());
}

TEST(CollatzFixture, solve4) {
    istringstream r("7884 9618\n900 8518\n1249 6234\n1069 7297\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("7884 9618 260\n900 8518 262\n1249 6234 262\n1069 7297 262\n", w.str());
}

TEST(CollatzFixture, solve5) {
    istringstream r("34008 77425\n19591 61467\n36729 62842\n32961 95966\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("34008 77425 351\n19591 61467 340\n36729 62842 340\n32961 95966 351\n", w.str());
}
